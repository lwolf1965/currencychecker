/*-----------------------------------------------------------------
 * Copyright (C) Zilvinas Mikaliunas All rights reserved.
 * Revision: 1.0
 * Author: zilvinas.mikaliunas 
 *-----------------------------------------------------------------*/
package com.zm.servlet;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import com.zm.servlet.data.ExchangeBase;
import com.zm.servlet.data.ExchangeBean;
import com.zm.servlet.services.LBRatesService;

public class HTMLPage
{
	private StringBuilder sb;
	private ExchangeBase  base;
	private Calendar requestDate;
	
	public HTMLPage( Calendar date )
	{
		sb = new StringBuilder();
		base =  new ExchangeBase();
		requestDate = date;
	}
	
	@Override
	public String toString( )
	{
		sb.append("<html lang=\"en-US\"><head><title>Currency rate comparator</title><head></head><body>");
		input( requestDate );
		result( requestDate );
		sb.append("</body></html>");
		return sb.toString();
	}
	
	private void input(Calendar date)
	{
		DateForm df = new DateForm( date );
		sb.append( df );
	}
	
	private void result( Calendar date )
	{
		String sCurrent;
		String sBefore;
		
		sb.append("<table border = \"1\" width =\"600\" >");
		LBRatesService dr = new LBRatesService();
		
		sCurrent = generateDayString( date);
		List<ExchangeBean> current = dr.getExcangeRates( sCurrent );
		
		date.add(Calendar.DAY_OF_YEAR, -1);
		sBefore = generateDayString( date);
		base.addAll( dr.getExcangeRates( sBefore ) );
		for (ExchangeBean eb : current)
		{
			ExchangeBean prev = base.getCurrencyData( eb.getCurrency() );
			if( prev != null)
			{
				eb.calculateDiff( prev.getRate() );
			}
		}
		base.clear();
		base = null;
		Collections.sort( current );
		
		sb.append("<p> Comparision curency excange rates between : ").append(sBefore).append(" and ").append(sCurrent).append("</p");
		
		printData( current );
		sb.append("</table>");
	}
	
	private void printData( List<ExchangeBean> data )
	{
		for( ExchangeBean eb : data )
		{
			sb.append( eb.toString() );
		}
	}
	

	private String generateDayString( Calendar date )
	{
		StringBuilder sb = new StringBuilder();
		sb.append( date.get( Calendar.YEAR )).append("-");
		sb.append( date.get( Calendar.MONTH ) + 1 ).append("-"); // Zero based month
		sb.append( date.get( Calendar.DAY_OF_MONTH ));
		return sb.toString();
	}
}
