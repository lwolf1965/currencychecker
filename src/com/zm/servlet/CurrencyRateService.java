/*-----------------------------------------------------------------
 * Copyright (C) Zilvinas Mikaliunas All rights reserved.
 * Revision: 1.0
 * Author: zilvinas.mikaliunas 
 *-----------------------------------------------------------------*/
package com.zm.servlet;

import java.io.IOException;
import java.util.Calendar;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zm.servlet.data.DateValidator;

@WebServlet("/CurrrencyRate")
public class CurrencyRateService extends HttpServlet
{
	private static final long serialVersionUID = -2582614211893164366L;

	public CurrencyRateService()
    {
        super();
    }

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		DateValidator dv = new DateValidator();
		Calendar date = dv.validate( request.getParameterMap() );
		HTMLPage html = new HTMLPage(date);
		response.getWriter().append( html.toString() );
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}
