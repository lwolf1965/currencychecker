/*-----------------------------------------------------------------
 * Copyright (C) Zilvinas Mikaliunas All rights reserved.
 * Revision: 1.0
 * Author: zilvinas.mikaliunas 
 *-----------------------------------------------------------------*/
package com.zm.servlet;

import java.util.Calendar;

public class DateForm
{
	public static final int YEAR_BEGIN = 1994;
	public static final int YEAR_END = 2015 ;
	private static final String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	
	private StringBuilder sb = new StringBuilder();
	private Calendar formDate;
	
	public DateForm(Calendar date)
	{
		formDate = date;
	}
	
	@Override
	public String toString()
	{
		sb.append("<form action=\"/CurrrencyRate/CurrrencyRate\" method=\"get\">");
		sb.append("<table border =\"0\" width = \"400\"><tr>");
		year( formDate.get( Calendar.YEAR ) );
		month( formDate.get( Calendar.MONTH) );
		day(formDate.get( Calendar.DAY_OF_MONTH )  );
		button();
		sb.append("</tr></table></form>");
		return sb.toString();
	}
	
	private void year(int select)
	{
		sb.append("<td>Year : </td><td><select name =\"year\" size = \"1\">");
		for( int i = YEAR_BEGIN; i < YEAR_END; i++ )
		{
			sb.append("<option value =\"").append( i ).append("\""); 
			if( i == select)
			{
				sb.append(" selected");
			}
			sb.append(">").append( i ).append("</option>\r");
		}
		sb.append("</select></td>");
		
	}

	private void month(int select)
	{
		sb.append("<td>Month : </td><td><select name =\"month\" size = \"1\">");;
		for( int i = 0; i < 12; i++ )
		{
			sb.append("<option value =\"").append( i ).append("\""); 
			if( i == select)
			{
				sb.append(" selected");
			}
			sb.append(">").append( months[i] ).append("</option>\r");
		}
		sb.append("</select></td>");
	}

	private void day(int select)
	{
		sb.append("<td>Day : </td><td><select name =\"day\" size = \"1\">");;
		for( int i = 1; i < 32; i++ )
		{
			sb.append("<option value =\"").append( i ).append("\""); 
			if( i == select)
			{
				sb.append(" selected");
			}
			sb.append(">").append( i ).append("</option>\r");
		}
		sb.append("</select></td>");
	}
	
	private void button()
	{
		sb.append("<td><input type=\"submit\" name=\"submit\" value=\"Check\"><td>");
	}
}
