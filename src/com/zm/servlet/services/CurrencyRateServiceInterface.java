/*-----------------------------------------------------------------
 * Copyright (C) Zilvinas Mikaliunas All rights reserved.
 * Revision: 1.0
 * Author: zilvinas.mikaliunas
 * -----------------------------------------------------------------*/
package com.zm.servlet.services;

import java.util.List;

import com.zm.servlet.data.ExchangeBean;

public interface CurrencyRateServiceInterface
{
	 List<ExchangeBean> getExcangeRates( String date);
}
