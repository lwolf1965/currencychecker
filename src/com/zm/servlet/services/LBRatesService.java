/*-----------------------------------------------------------------
 * Copyright (C) Zilvinas Mikaliunas All rights reserved.
 * Revision: 1.0
 * Author: zilvinas.mikaliunas 
 *-----------------------------------------------------------------*/
package com.zm.servlet.services;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.zm.servlet.data.ExchangeBean;

public class LBRatesService implements CurrencyRateServiceInterface
{
	public static final String BASE_URL = "http://www.lb.lt/webservices/ExchangeRates/ExchangeRates.asmx/getExchangeRatesByDate?Date=";
	public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.29 Safari/537.36";
	public static final int TIMEOUT_MS = 30000; 
	
	public LBRatesService()
	{
		
	}
	
	@Override
	public List<ExchangeBean> getExcangeRates( String date)
	{
		List <ExchangeBean > l = new ArrayList<>();
		try
		{
			URL url =  new URL( BASE_URL + date);
			HttpURLConnection uc = (HttpURLConnection) url.openConnection();
			uc.setDoOutput( true );
			uc.setRequestProperty( "User-Agent", USER_AGENT );
			uc.setRequestMethod("GET");
			uc.setReadTimeout( TIMEOUT_MS );
			uc.connect();
			InputStream is =  uc.getInputStream();
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = docFactory.newDocumentBuilder();
			Document ratesXML = dBuilder.parse( is );
			ratesXML.getDocumentElement().normalize();
			
			NodeList nl = ratesXML.getElementsByTagName("ExchangeRates");
			if( nl != null)
			{
				NodeList inl =  nl.item(0).getChildNodes();
				for( int i = 0;  i < inl.getLength(); i++  )
				{
					Node n = inl.item( i );
					ExchangeBean eb = parseItems( n.getChildNodes() );
					if( eb != null)
					{
						l.add( eb );
					}
				}
				is.close();	
			}
		}
		catch (Throwable e)
		{
			e.printStackTrace();
		}
		return l;
	}

	private ExchangeBean parseItems(NodeList nodesList)
	{
		ExchangeBean eb = new ExchangeBean();
		for( int i = 0; i < nodesList.getLength(); i++  )
		{
			Node n = nodesList.item( i );
			String name =  n.getNodeName();
			switch( name )
			{
				case "currency": 
					eb.setCurrency(n.getTextContent());
					break;
				case "rate"	:
					eb.setRate(n.getTextContent());
					break;
			}
		}
		if(  eb.getCurrency() == null )
		{
			return null;
		}
		return eb;
	}

}
