/*-----------------------------------------------------------------
 * Copyright (C) Zilvinas Mikaliunas All rights reserved.
 * Revision: 1.0
 * Author: zilvinas.mikaliunas 
 *-----------------------------------------------------------------*/
package com.zm.servlet.data;

import java.util.Calendar;
import java.util.Map;

public class DateValidator
{

	public DateValidator()
	{
	}
	
	public Calendar validate( Map<String, String[]> reqp )
	{
		Calendar cal = Calendar.getInstance();
		String[] yearStr = reqp.get("year");
		String[] monthStr = reqp.get("month");
		String[] dayStr = reqp.get("day");
	
		try
		{
			int year = Integer.parseInt( yearStr[0] );
			int month = Integer.parseInt( monthStr[0] );
			int day = Integer.parseInt( dayStr[0] );
			cal.set(year, month, day);
		}
		catch(Exception e)
		{
			cal.set(2014, 11, 31); // last day of Litas :-(;
		}
		return cal;
	}
}
