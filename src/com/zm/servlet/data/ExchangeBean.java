/*-----------------------------------------------------------------
 * Copyright (C) Zilvinas Mikaliunas All rights reserved.
 * Revision: 1.0
 * Author: zilvinas.mikaliunas 
 *-----------------------------------------------------------------*/
package com.zm.servlet.data;

import java.text.DecimalFormat;

public class ExchangeBean implements Comparable<ExchangeBean>
{
	private String name;
	private double rate;
	private double diff;
	
	public ExchangeBean()
	{
		diff = 0.0D;
	}
	
	@Override
	public int compareTo(ExchangeBean o)
	{
		if( o instanceof ExchangeBean )
		{
			if( o.getDiff() > diff)
			{
				return 1;
			}
			else if ( o.getDiff() < diff )
			{
				return -1;
			}
			else
			{
				return 0;
			}
		}
		else
		{
			throw( new ClassCastException() );
		}
	}

	public void setCurrency(String text)
	{
		name = text;
	}

	public void setRate(String text)
	{
		rate = Double.parseDouble( text );
	}
	
	@Override
	public String toString()
	{
		DecimalFormat df = new DecimalFormat("####.####");
		StringBuilder sb = new StringBuilder();
		sb.append("<tr>");
		sb.append("<td>Currency</td>");
		sb.append("<td>").append(name).append("</td>");
		sb.append("<td>Rate</td>");
		sb.append("<td>").append( df.format(rate) ).append("</td>");
		sb.append("<td>Change</td>");
		sb.append("<td>").append( df.format(diff) ).append("</td>");
		sb.append("</tr>\n");
		return sb.toString();
	}

	public String getCurrency()
	{
		return name;
	}

	public double getRate()
	{
		return rate;
	}

	public void calculateDiff( double rate2 )
	{
		diff = rate - rate2;
	}
	
	public double getDiff()
	{
		return diff;
	}
}
