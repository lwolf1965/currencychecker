/*-----------------------------------------------------------------
 * Copyright (C) Zilvinas Mikaliunas All rights reserved.
 * Revision: 1.0
 * Author: zilvinas.mikaliunas 
 *-----------------------------------------------------------------*/
package com.zm.servlet.data;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class ExchangeBase
{
	private Map<String, ExchangeBean> map;
	
	public ExchangeBase()
	{
		map = new ConcurrentHashMap<>();
	}
	
	public void addAll(List< ExchangeBean > list)
	{
		for( ExchangeBean eb : list)
		{
			map.put(eb.getCurrency(), eb );
		}
	}
	
	public ExchangeBean getCurrencyData( String currency )
	{
		return map.get( currency );
	}

	public void clear()
	{
		map.clear();
	}
}

